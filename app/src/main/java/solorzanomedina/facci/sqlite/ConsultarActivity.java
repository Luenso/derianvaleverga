package solorzanomedina.facci.sqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ConsultarActivity extends AppCompatActivity {
    EditText cedula;
    TextView nombres, apellidos, ciudad, fechaNaci, facultad;
    Button buscar;

    SQLiteHelper conexion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar);
        cedula = findViewById(R.id.txtCedula);
        nombres = findViewById(R.id.txtNombre);
        apellidos = findViewById(R.id.txtApellidos);
        ciudad = findViewById(R.id.txtCuidad);
        fechaNaci = findViewById(R.id.txtFecha);
        facultad = findViewById(R.id.txtFacultad);
        buscar = findViewById(R.id.btnbuscar);

        conexion= new SQLiteHelper(getApplicationContext(),"basedatos", null, 1);


        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                consultar();
            }
        });
    }

    private void consultar() {
        SQLiteDatabase db = conexion.getReadableDatabase();

        String[] parametro = new String[]{cedula.getText().toString()};
        String[]  campos = new String[]{"cedula", "nombre", "apellidos","ciudad_naci","fecha_naci","facultad"};

        try{
            Cursor cursor = db.query("estudiante", campos, "cedula = ?", parametro, null, null,null);
            cursor.moveToFirst();
            nombres.setText(cursor.getString(1));
            apellidos.setText(cursor.getString(2));
            ciudad.setText(cursor.getString(3));
            fechaNaci.setText(cursor.getString(4));
            facultad.setText(cursor.getString(5));
            cursor.close();


        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "El documento no exite", Toast.LENGTH_SHORT).show();

        }


    }

    private void consultarSentenciasSql() {
        SQLiteDatabase db = conexion.getReadableDatabase();
        String[] parametro = {cedula.getText().toString()};

        String consulta = "SELECT cedula, nombre, apellidos, ciudad_nac, fecha_nac, facultad FROM estudiante WHERE cedula =?";

        try {
            Cursor cursor = db.rawQuery(consulta,parametro);

            nombres.setText(cursor.getString(1));
            apellidos.setText(cursor.getString(2));
            ciudad.setText(cursor.getString(3));
            fechaNaci.setText(cursor.getString(4));
            facultad.setText(cursor.getString(5));
            cursor.moveToFirst();
            cursor.close();
        }catch (Exception e){}


    }


}
