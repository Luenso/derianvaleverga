package solorzanomedina.facci.sqlite;

public class Estudiante {
       private String cedula;
       private String nombres;
       private String apellidos;
       private String ciudad_naci;
       private String fecha_naci;
       private String facultad;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCiudad_naci() {
        return ciudad_naci;
    }

    public void setCiudad_naci(String ciudad_naci) {
        this.ciudad_naci = ciudad_naci;
    }

    public String getFecha_naci() {
        return fecha_naci;
    }

    public void setFecha_naci(String fecha_naci) {
        this.fecha_naci = fecha_naci;
    }

    public String getFacultad() {
        return facultad;
    }

    public void setFacultad(String facultad) {
        this.facultad = facultad;
    }
}
