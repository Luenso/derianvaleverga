package solorzanomedina.facci.sqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {
    ListView listviewEstudiante;
    ArrayList<String> listaInformacion;
    ArrayList<Estudiante> listaEstudiante;

    SQLiteHelper conexion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        listviewEstudiante = findViewById(R.id.ListViewEstudiante);

       conexion = new SQLiteHelper(getApplicationContext(),"basedatos", null, 1);
        consultarListaEstudiante();
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,listaInformacion);
        listviewEstudiante.setAdapter(arrayAdapter);
    }

    private void consultarListaEstudiante() {
        SQLiteDatabase db = conexion.getReadableDatabase();
        Estudiante estudiante = null;
        listaEstudiante = new ArrayList<Estudiante>();

        Cursor cursor = db.rawQuery("SELECT * FROM estudiante",null);

            while(cursor.moveToNext()){
                estudiante = new Estudiante();
                estudiante.setCedula(cursor.getString(0));
                estudiante.setNombres(cursor.getString(1));
                estudiante.setApellidos(cursor.getString(2));
                estudiante.setCiudad_naci(cursor.getString(3));
                estudiante.setFecha_naci(cursor.getString(4));
                estudiante.setFacultad(cursor.getString(5));
                listaEstudiante.add(estudiante);

            }
            obtenerLista();
    }

    private void obtenerLista() {
        listaInformacion =new ArrayList<String>();

        for (int i = 0; i<listaEstudiante.size();i++){
            listaInformacion.add(listaEstudiante.get(i).getCedula() +
                    "\n "+listaEstudiante.get(i).getNombres()+
                    "\n "+listaEstudiante.get(i).getApellidos()+
                    "\n "+listaEstudiante.get(i).getCiudad_naci()+
                    "\n "+listaEstudiante.get(i).getFecha_naci()+
                    "\n "+listaEstudiante.get(i).getFacultad());
        }
    }
}
