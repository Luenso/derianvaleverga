package solorzanomedina.facci.sqlite;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistroActivity extends AppCompatActivity {
    EditText cedula, nombre, apellidos, ciudadNac, fechaNaci, facultad;
    Button registrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        cedula = findViewById(R.id.etCedula);
        nombre = findViewById(R.id.etNombres);
        apellidos = findViewById(R.id.etApellidos);
        fechaNaci = findViewById(R.id.etFechaNac);
        ciudadNac = findViewById(R.id.etCuidad);
        facultad = findViewById(R.id.etFacultad);
        registrar = findViewById(R.id.btRegistrar);

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registroEstudianteSentenciasSql();
            }
        });
    }

    private void registroEstudiante() {
        SQLiteHelper conexion = new SQLiteHelper(this, "basedatos", null,1);

        SQLiteDatabase db = conexion.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("cedula", cedula.getText().toString());
        contentValues.put("nombre", nombre.getText().toString());
        contentValues.put("apellidos", apellidos.getText().toString());
        contentValues.put("cuidad_naci", ciudadNac.getText().toString());
        contentValues.put("fecha_naci", fechaNaci.getText().toString());
        contentValues.put("facultad", facultad.getText().toString());

        Long id  = db.insert("estudiante", "cedula",contentValues);
        db.close();

        Toast.makeText(getApplicationContext(), "Se ha registrado con exito " +id, Toast.LENGTH_SHORT).show();
        limpiarText();

    }
    private void registroEstudianteSentenciasSql(){
        SQLiteHelper conexion = new SQLiteHelper(this, "basedatos", null,1);

        SQLiteDatabase db = conexion.getWritableDatabase();

        String insert = "INSERT INTO estudiante(cedula, nombre, apellidos, ciudad_naci, fecha_naci, facultad)" +
                " VALUES('"+cedula.getText().toString()+ "', '"+nombre.getText().toString()+ "', '"+apellidos.getText().toString()+ "', '"
                +ciudadNac.getText().toString()+ "', '"+fechaNaci.getText().toString()+ "', '"+facultad.getText().toString()+"')";
        db.execSQL(insert);

        db.close();
        Toast.makeText(getApplicationContext(), "Se ha registrado con exito", Toast.LENGTH_SHORT).show();
        limpiarText();

    }
    public void limpiarText(){
        cedula.setText("");
        nombre.setText("");
        apellidos.setText("");
        fechaNaci.setText("");
        ciudadNac.setText("");
        facultad.setText("");
    }
}
